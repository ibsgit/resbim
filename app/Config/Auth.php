<?php

namespace Config;

class Auth extends \Myth\Auth\Config\Auth
{
	public $defaultUserGroup = 'user';
	public $views = [
		'login'		   		=> '\App\Views\auth\login',
		'register'			=> '\App\Views\auth\register',
		'forgot'		  	=> '\App\Views\auth\forgot',
		'reset'		   		=> '\App\Views\auth\reset',
		'emailForgot'	 	=> '\App\Views\auth\emails\forgot',
		'emailActivation' 	=> '\App\Views\auth\emails\activation',

		// 'login'		   => 'Myth\Auth\Views\login',
		// 'register'		=> 'Myth\Auth\Views\register',
		// 'forgot'		  => 'Myth\Auth\Views\forgot',
		// 'reset'		   => 'Myth\Auth\Views\reset',
		// 'emailForgot'	 => 'Myth\Auth\Views\emails\forgot',
		// 'emailActivation' => 'Myth\Auth\Views\emails\activation',
	];

	public $allowRemembering = true;
	public $rememberLength = 30 * DAY;
	public $minimumPasswordLength = 8;
	public $requireActivation = 'Myth\Auth\Authentication\Activators\EmailActivator';
}
