<?php

namespace App\Controllers;

use Myth\Auth\Password;

class Front extends BaseController
{
	protected $db;
	public function __construct()
	{
		$this->db      = \Config\Database::connect();
		$this->request = service('request');
		helper('encrypt');
	}

	public function faq()
	{

		$data['title'] = 'Data Permasalahan';
		$faq = new \App\Models\Resbim\FAQModel();
		$data['faq'] = $faq
			->orderBy('order', 'asc')
			->orderBy('created_at', 'desc')
			->findAll();
		return view('front/index', $data);
	}
}
