<?php

namespace App\Controllers;

use Myth\Auth\Password;

class Resbim extends BaseController
{
	protected $db;
	public function __construct()
	{
		$this->db      = \Config\Database::connect();
		$this->request = service('request');
		helper('encrypt');
	}

	public function index()
	{
		return redirect()->to('list');
	}

	public function category()
	{
		$data['title'] = 'Kategori';
		$category = new \App\Models\Resbim\CategoryModel();
		$data['categories'] = $category->findAll();

		return view('resbim/category', $data);
	}

	public function categoryDetail()
	{
		$data['title'] = 'Kategori (Detail)';
		// $detail = new \App\Models\Resbim\CategoryDetailModel();
		// $data['detail'] = $detail->findAll();


		$builder = $this->db->table('resbim_category_detail');
		$builder->select('resbim_category_detail.*, resbim_category.name as category');
		$builder->join('resbim_category', 'resbim_category.id = resbim_category_detail.categoryid', 'left');
		$builder->orderBy('resbim_category.order');
		$builder->orderBy('resbim_category_detail.order');
		$query = $builder->get();
		$data['detail'] = $query->getResult();
		// d($data['detail']);

		return view('resbim/categoryDetail', $data);
	}

	/////////////////////////////////////////////            PERMASALAHAN
	public function faq($act = null, $id = 0)
	{
		$rules = [
			'question'		=> [
				'label' => 'Permasalahan',
				'rules' => 'required'
			],
			'answer'		=> [
				'label' => 'jawaban',
				'rules' => 'required'
			],
		];

		if ($act == 'add') {
			if ($dataPost = $this->request->getPost()) {
				$FAQModel = new \App\Models\Resbim\FAQModel();

				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if (!$FAQModel->save($dataPost)) {
					return redirect()->back()->withInput()->with('errors', $FAQModel->errors());
				}
				return redirect()->to('/resbim/faq')->with('message', 'Berhasil menambah data.');
			}
			$data['title'] = 'Tambah Data Permasalahan';
			return view('resbim/faq_add', $data);
		} elseif ($act == 'edit') {
			if ($dataPost = $this->request->getPost()) {
				$FAQModel = new \App\Models\Resbim\FAQModel();

				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if (!$FAQModel->save($dataPost)) {
					return redirect()->back()->withInput()->with('errors', $FAQModel->errors());
				}
				return redirect()->to('/resbim/faq')->with('message', 'Berhasil menyimpan perubahan.');
			}
			$FAQModel = new \App\Models\Resbim\FAQModel();
			$data['faq'] = $FAQModel
				->find($id);

			$data['title'] = 'Ubah Data Permasalahan';
			return view('resbim/faq_add', $data);
		} elseif ($act == 'delete') {
			if (is_numeric($id = decrypt($id))) {
				$FAQModel = new \App\Models\Resbim\FAQModel();
				if (!$FAQModel->delete($id)) {
					return redirect()->back()->withInput()->with('errors', $FAQModel->errors());
				}
				return redirect()->to('/resbim/faq')->with('message', 'Berhasil manghapus data.');
			}
		} else {
			$data['title'] = 'Data Permasalahan';
			$faq = new \App\Models\Resbim\FAQModel();
			$data['faq'] = $faq
				->orderBy('order', 'asc')
				->orderBy('created_at', 'desc')
				->findAll();
			return view('resbim/faq', $data);
		}
	}


	/////////////////////////////////////////////            KONSULTASI
	public function qna($act = null, $id = 0)
	{
		$rules = [
			'name'		=> [
				'label' => 'Judul',
				'rules' => 'required'
			],
			'detailcategoryid'		=> [
				'label' => 'Kategori',
				'rules' => 'required'
			],
			'content'		=> [
				'label' => 'Pertanyaan',
				'rules' => 'required'
			],
		];

		if ($act == 'add') {  /////// TAMBAH KONSULTASI
			if ($dataPost = $this->request->getPost()) {

				$QnAModel = new \App\Models\Resbim\QnAModel();
				$QnADetailModel = new \App\Models\Resbim\QnADetailModel();

				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if (!$QnAModel->save($dataPost)) {
					return redirect()->back()->withInput()->with('errors', $QnAModel->errors());
				} else {
					$dataPostDetail['content'] = $dataPost['content'];
					$dataPostDetail['qnaid'] = $QnAModel->insertID();
					$dataPostDetail['userid'] = user()->id;
					if (!$QnADetailModel->save($dataPostDetail)) {
						return redirect()->back()->withInput()->with('errors', $QnADetailModel->errors());
					}
				}


				//jika yang posting bukan admin, kirim notif ke nomor
				if (!in_groups('admin')) {
					$identitas = user()->fullname . ' (' . user()->email . ')';
					$pembukaan = "Terdapat konsultasi baru dari $identitas pada aplikasi *e-Resbim*:
					
";
					helper('whatsapp');
					echo sendWA('6285299504623', $pembukaan . $dataPost['content']);
				}
				return redirect()->to('/resbim/qna')->with('message', 'Berhasil menambah data.');
			}
			$data['title'] = 'Buka Konsultasi Baru';

			$builder = $this->db->table('resbim_category_detail');
			$builder->select('resbim_category_detail.*, resbim_category.name as category');
			$builder->join('resbim_category', 'resbim_category.id = resbim_category_detail.categoryid', 'left');
			$builder->orderBy('resbim_category.order');
			$builder->orderBy('resbim_category_detail.order');
			$query = $builder->get();
			$data['category'] = $query->getResult();
			// d($data['detail']);

			return view('resbim/qna_add', $data);
		} elseif ($act == 'delete') {
			if (is_numeric($id = decrypt($id))) {
				$FAQModel = new \App\Models\Resbim\FAQModel();
				if (!$FAQModel->delete($id)) {
					return redirect()->back()->withInput()->with('errors', $FAQModel->errors());
				}
				return redirect()->to('/resbim/qna')->with('message', 'Berhasil manghapus data.');
			}
		} elseif ($act == 'close') {
			if (is_numeric($id = decrypt($id))) {
				$QnAModel = new \App\Models\Resbim\QnAModel();
				$UserModel = new \App\Models\Auth\UserModel();
				$dataClose['status'] = 3;
				$dataClose['id'] = $id;

				if (!$QnAModel->save($dataClose)) {
					return redirect()->back()->withInput()->with('errors', $QnAModel->errors());
				}
				$dataQnA = $QnAModel->find($id);
				$dataUser = $UserModel->find($dataQnA->userid);
				helper('whatsapp');
				//$dataUser->whatsapp
				sendWA($dataUser->whatsapp, "
Terima kasih Bapak/Ibu *$dataUser->fullname* telah menggunakan layanan konsultasi melalui aplikasi e-Resbim Kanreg VIII BKN.

Bersama dengan ini tiket konsultasi dengan judul 
*$dataQnA->name* 
telah kami tutup, semoga jawaban dari kami dapat bermanfaat bagi Bapak/Ibu. Kami mohon kepada Bapak/Ibu agar berkenan untuk mengisi survei kepuasan pelanggan atas pelayanan kami melalui bit.ly/IKMpembinaanbidang.

Atas perhatian dan waktu dari Bapak/Ibu kami ucapkan terima kasih.

Salam,

*Tim Pelayanan e-Resbim*
*Kanreg VIII BKN*
				");

				return redirect()->to('/resbim/qna')->with('message', 'Berhasil menutup konsultasi.');
			}
		} elseif ($act == 'open') {
			if (is_numeric($id = decrypt($id))) {
				$QnAModel = new \App\Models\Resbim\QnAModel();
				$dataClose['status'] = 2;
				$dataClose['id'] = $id;

				if (!$QnAModel->save($dataClose)) {
					return redirect()->back()->withInput()->with('errors', $QnAModel->errors());
				}
				return redirect()->to('/resbim/qna')->with('message', 'Berhasil membuka konsultasi.');
			}
		} else {
			$data['title'] = 'Data Konsultasi';

			$builder = $this->db->table('resbim_qna');
			$builder->select('resbim_qna.*, users.fullname as userfullname, users.username as username, resbim_category_detail.name as subcategory, resbim_category.name as category');
			$builder->join('resbim_category_detail', 'resbim_qna.detailcategoryid = resbim_category_detail.id');
			$builder->join('resbim_category', 'resbim_category_detail.categoryid = resbim_category.id');
			$builder->join('users', 'resbim_qna.userid = users.id');
			if (in_groups('user')) {
				$builder->where('resbim_qna.userid', user()->id);
				$builder->orderBy('resbim_qna.status');
				$builder->orderBy('resbim_qna.updated_at', 'desc');
				$builder->orderBy('resbim_qna.created_at', 'desc');
			} else {
				$builder->orderBy('resbim_qna.status');
				$builder->orderBy('resbim_qna.updated_at', 'desc');
				$builder->orderBy('resbim_qna.created_at', 'desc');
			}

			$query = $builder->get();
			$data['qna'] = $query->getResult();
			// d($data['qna']);
			return view('resbim/qna', $data);
		}
	}
	/////////////////////////////////////////////            KONSULTASI
	public function qna_detail($act = null, $id = 0)
	{
		$rules = [
			'content'		=> [
				'label' => 'Jawaban',
				'rules' => 'required'
			],
		];

		if ($act == 'add') {
			if ($dataPost = $this->request->getPost()) {
				$QnADetailModel = new \App\Models\Resbim\QnADetailModel();
				$QnAModel = new \App\Models\Resbim\QnAModel();

				$dataPost['qnaid'] = decrypt($id);
				$dataPost['userid'] = user()->id;

				$updateQnA['status'] = $dataPost['status'];
				$updateQnA['id'] = $dataPost['qnaid'];
				d($dataPost);

				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if (!$QnADetailModel->insert($dataPost) || !$QnAModel->save($updateQnA)) {
					return redirect()->back()->withInput()->with('errors', $QnADetailModel->errors());
				}


				$QnAModel = new \App\Models\Resbim\QnAModel();
				$UserModel = new \App\Models\Auth\UserModel();

				$dataQnA = $QnAModel->find($dataPost['qnaid']);
				$dataUser = $UserModel->find($dataQnA->userid);


				//jika yang posting bukan admin, kirim notif ke nomor
				if (!in_groups('admin')) {
					$identitas = user()->fullname . ' (' . user()->email . ')';
					$pembukaan = "Terdapat respon baru dari $identitas pada aplikasi *e-Resbim*:
					
";
					helper('whatsapp');
					sendWA('6285299504623', $pembukaan . $dataPost['content']);
				}

				//jika yang menjawab adalah selain pemilik nomor
				if ($dataQnA->userid != user()->id) {

					helper('whatsapp');
					sendWA($dataUser->whatsapp, "

Terimakasih Bapak/Ibu *$dataUser->fullname* pertanyaan Anda sudah dijawab melalui aplikasi eRESBIM. Mohon dukungan penerapan Zona Integritas Menuju Wilayah Bebas dari Korupsi (WBK) & Wilayah Birokrasi Bersih dan Melayani (WBBM) pada Kantor Regional VIII BKN.
Klik Link bit.ly/IKMpembinaanbidang untuk mengisi survey atas pelayanan kami. Terimakasih.
	
Pesan ini dikirim secara otomatis melalui aplikasi eRESBIM sebagai notifikasi konsultasi yang diajukan.
Terimakasih telah menggunakan aplikasi ini.

Salam,
Kanreg VIII BKN
");
				}
				return redirect()->back()->with('message', 'Berhasil menambah data.');
			} else {
				return redirect()->back();
			}
		} else {
			if (is_numeric($id = decrypt($id))) {
				$data['title'] = 'Detail Konsultasi';

				$builder = $this->db->table('resbim_qna');
				$builder->select('resbim_qna.*, users.fullname as userfullname, users.username as username, resbim_category_detail.name as subcategory, resbim_category.name as category');
				$builder->join('resbim_category_detail', 'resbim_qna.detailcategoryid = resbim_category_detail.id');
				$builder->join('resbim_category', 'resbim_category_detail.categoryid = resbim_category.id');
				$builder->join('users', 'resbim_qna.userid = users.id');
				$builder->where('resbim_qna.id', $id);
				$query = $builder->get();
				$data['qna'] = $query->getRow();
				$data['qna']->crypt = encrypt($data['qna']->id);

				$builder = $this->db->table('resbim_qna_detail');
				$builder->select('resbim_qna_detail.*, users.fullname as userfullname, users.username as username');
				$builder->join('users', 'resbim_qna_detail.userid = users.id');
				$builder->orderBy('resbim_qna_detail.updated_at', 'asc');
				$builder->orderBy('resbim_qna_detail.created_at', 'asc');
				$builder->where('resbim_qna_detail.qnaid', $id);
				$query = $builder->get();
				$data['detail'] = $query->getResult();

				// d($data);
				return view('resbim/qna_detail', $data);
			}
		}
	}
}
