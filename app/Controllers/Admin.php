<?php

namespace App\Controllers;

use Myth\Auth\Password;

class Admin extends BaseController
{
	protected $db;
	public function __construct()
	{
		$this->db      = \Config\Database::connect();
		$this->request = service('request');
	}
	public function index()
	{
		return redirect()->to('list');
	}
	public function list()
	{
		$data['title'] = 'List Pengguna';
		// $users = new \Myth\Auth\Models\UserModel();
		// $data['users'] = $users->findAll();

		$builder = $this->db->table('users');
		$builder->select('users.id as userid,username, email,auth_groups.id as groupid, auth_groups.name as groupname');
		$builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
		$builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
		$query = $builder->get();
		$data['users'] = $query->getResult();


		return view('admin/list', $data);
	}

	public function detail($id = 0)
	{
		helper('encrypt');
		if ($dataPost = $this->request->getPost()) {
			if ($dataPost['id'] == $id && decrypt($dataPost['crypt']) == $id) {
				$userModel = new \App\Models\Auth\UserModel();
				$groupModel = new \App\Models\Auth\GroupModel();
				$group = $dataPost['group'];
				unset($dataPost['group']);

				$rules = [
					'whatsapp'			=> 'numeric|permit_empty',
				];
				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if (!$userModel->save($dataPost)) {
					return redirect()->back()->withInput()->with('errors', $userModel->errors());
				}
				$groupModel->removeUserFromAllGroups($id);
				$groupModel->addUserToGroup($id, $group);
				return redirect()->back()->with('message', 'Berhasil menyimpan perubahan.');
			} else {
				return redirect()->back()->withInput()->with('errors', 'Kesalahan ID');
			}
		} else {
			$data['title'] = 'Detail Pengguna';

			$builder = $this->db->table('users');
			$builder->select('users.id as userid,username, email,auth_groups.id as groupid, auth_groups.name as groupname, fullname, user_image, whatsapp');
			$builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
			$builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
			$builder->where('users.id', $id);
			$query = $builder->get();
			$data['user'] = $query->getRow();

			if (empty($data['user'])) {
				return redirect()->to('list');
			}

			$data['user']->crypt = encrypt($id);
			$builder = $this->db->table('auth_groups');
			$builder->select('*');
			$query = $builder->get();
			$data['groups'] = $query->getResult();

			return view('admin/detail', $data);
		}
	}


	public function profile($id = null)
	{
		helper('encrypt');
		$data['user'] = user();

		if ($dataPost = $this->request->getPost()) {
			if ($dataPost['id'] == $id && decrypt($dataPost['crypt']) == $id) {
				$userModel = new \App\Models\Auth\UserModel();
				$rules = [
					'whatsapp'		=> [
						'label' => 'Whatsapp',
						'rules' => 'required|numeric'
					],
					'fullname'		=> [
						'label' => 'Nama lengkap',
						'rules' => 'required'
					],
					'password'		=> [
						'label' => 'Kata sandi',
						'rules' => 'strong_password|permit_empty|required_with[pass_confirm]'
					],
					'pass_confirm'		=> [
						'label' => 'Ulangi kata Sandi',
						'rules' => 'matches[password]|required_with[password]'
					],
				];
				if (!$this->validate($rules)) {
					return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
				}
				if ($dataPost['password'] != '') {
					$dataPost['password_hash'] = Password::hash($dataPost['password']);
				}
				if (!$userModel->save($dataPost)) {
					return redirect()->back()->withInput()->with('errors', $userModel->errors());
				}
				return redirect()->back()->with('message', 'Berhasil menyimpan perubahan.');
			} else {
				return redirect()->back()->withInput()->with('errors', 'Kesalahan ID');
			}
		} else {
			$data['title'] = 'Profil Saya';
			$data['user']->crypt = encrypt($data['user']->id);
			return view('admin/profile', $data);
		}
	}
}
