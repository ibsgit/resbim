<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		return view('auth/emails/activation');
	}

	public function sendWA($nomor, $pesan)
	{
		helper('whatsapp');
		sendWA($nomor, $pesan);
	}

	public function now()
	{
		echo date('Y-m-d ');
	}
}
