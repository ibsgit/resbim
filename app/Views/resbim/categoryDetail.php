<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Detail</th>
                            <th>Kategori</th>
                            <th>Urutan</th>
                            <th class="text-center" style="width: 140px;">
                                <a href="<?= base_url('/resbim/categoryDetail/add'); ?>" class="btn btn-sm btn-primary">tambah</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($detail as $ctg) : ?>
                            <tr>
                                <td style="width: 5%;"><?= $i++; ?></td>
                                <td><?= $ctg->name; ?></td>
                                <td><?= $ctg->category; ?></td>
                                <td><?= $ctg->order; ?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('/resbim/categoryDetail/' . $ctg->id); ?>" class="btn btn-sm btn-info">edit</a>
                                    <a href="<?= base_url('/resbim/categoryDetail/' . $ctg->id); ?>" class="btn btn-sm btn-danger">delete</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>