<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kategori</th>
                            <th>Judul</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 140px;">
                                <?php if (in_groups('user')) : ?>
                                    <a href="<?= base_url('/resbim/qna/add'); ?>" class="btn btn-sm btn-primary">tambah</a>
                                <?php else : ?>
                                    Aksi
                                <?php endif; ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="grupBuka">
                        <?php $i = 1; ?>
                        <?php if (!empty($qna)) : ?>
                            <?php foreach ($qna as $ctg) : ?>
                                <tr>
                                    <td style="width: 5%;"><?= $i++; ?></td>
                                    <td>
                                        <span class="badge badge-light"><?= $ctg->category . ' - ' . $ctg->subcategory; ?></span><br />
                                        <span class="badge badge-primary"><?= (empty($ctg->userfullname) ? $ctg->username :  $ctg->userfullname); ?></span>
                                        <span class="badge badge-info"><?= $ctg->updated_at; ?></span>
                                    </td>
                                    <td><?= $ctg->name; ?></td>
                                    <td><?= $ctg->status == 1 ? '<span class="badge badge-secondary">Menunggu jawaban</span>' : ($ctg->status == 2 ? '<span class="badge badge-light">Sudah dijawab</span>' : '<span class="badge badge-dark">ditutup</span>'); ?></td>
                                    <td class="text-center text-decoration-none">
                                        <a href="<?= base_url('/resbim/qna_detail/view/' . encrypt($ctg->id)); ?>" class="btn btn-sm btn-warning">detail</a>
                                        <?php if (in_groups('admin')) : ?>
                                            <a href="<?= base_url('/resbim/qna/' . ($ctg->status == 3 ? 'open' : 'close') . '/' . encrypt($ctg->id)); ?>" class="btn btn-sm btn-<?= ($ctg->status == 3 ? 'success' : 'danger'); ?>" onclick="return confirm('Apakah anda yakin akan <?= ($ctg->status == 3 ? 'membuka' : 'menutup'); ?> konsultasi ini?')"><?= ($ctg->status == 3 ? 'buka' : 'tutup'); ?></a>
                                        <?php endif; ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>