<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post">
                <input type="hidden" name="userid" value="<?= user()->id; ?>">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" name="name" class="form-control  <?php if (session('errors.name')) : ?>is-invalid<?php endif ?>" value="<?= !empty($qna) ? $qna->name : old('name') ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Instansi</label>
                            <input type="text" name="instansi" class="form-control  <?php if (session('errors.instansi')) : ?>is-invalid<?php endif ?>" value="<?= !empty($qna) ? $qna->instansi : old('instansi') ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="detailcategoryid" class="form-control  <?php if (session('errors.detailcategoryid')) : ?>is-invalid<?php endif ?>">
                                <option value=""></option>
                                <?php foreach ($category as $ctg) : ?>
                                    <option value="<?= $ctg->id; ?>"><?= $ctg->category . ' | ' . $ctg->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Pertanyaan</label>
                    <textarea class="form-control  <?php if (session('errors.content')) : ?>is-invalid<?php endif ?>" name="content" id="content" rows="10"><?= !empty($qna) ? $qna->content : old('content') ?></textarea>
                </div>
                <a class="btn btn-secondary" href="<?= base_url('resbim/qna'); ?>">Batal</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>


<?= $this->section('js'); ?>
<?php if (in_groups('admin')) : ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/28.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#answer'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
<?php endif; ?>
<?= $this->endSection(); ?>