<?= $this->extend('templates/index'); ?>

<?= $this->section('css'); ?>
<!-- Custom styles for this page -->
<link href="<?php base_url(); ?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<?= $this->endSection(); ?> -->

<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?>
                <!-- <a class="btn btn-primary py-0" target="front" href="<?= base_url('/front/faq'); ?>"><small><i class="fab fa-internet-explorer"></i> &nbsp; lihat tampilan web</small></a></h1> -->
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 70%;">Permasalahan & Jawaban</th>
                            <?php if (in_groups('admin')) : ?>

                                <th>Urutan</th>
                                <th class="text-center" style="width: 140px;">
                                    <a href="<?= base_url('/resbim/faq/add'); ?>" class="btn btn-sm btn-primary">tambah</a>
                                </th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody id="grupBuka">
                        <?php $i = 1; ?>
                        <?php foreach ($faq as $ctg) : ?>
                            <tr>
                                <td>
                                    <span class="strong" data-toggle="collapse" href="#buka<?= $ctg->id; ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <span class="text-dark"><?= $ctg->question; ?> </span>
                                    </span>
                                    <div class="collapse pt-2" id="buka<?= $ctg->id; ?>" data-parent="#grupBuka">
                                        <span class="text-primary"><?= $ctg->answer; ?></span>
                                    </div>
                                </td>
                                <?php if (in_groups('admin')) : ?>
                                    <td><?= $ctg->order; ?></td>
                                    <td class="text-center">
                                        <a href="<?= base_url('/resbim/faq/edit/' . $ctg->id); ?>" class="btn btn-sm btn-info">edit</a>
                                        <a href="<?= base_url('/resbim/faq/delete/' . encrypt($ctg->id)); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')">delete</a>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('js'); ?>
<script>
    $(document).ready(function() {
        var table = $('#dataTable').DataTable({
            drawCallback: function() {
                var api = this.api();
                var rowCount = api.rows({
                    page: 'current'
                }).count();

            }
        });

    });
</script>


<?= $this->endSection(); ?> -->