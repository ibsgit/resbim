<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post">
                <?php if (!empty($faq)) : ?>
                    <input type="hidden" name="id" value="<?= $faq->id; ?>">
                <?php endif; ?>
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label>Permasalahan</label>
                            <input type="text" name="question" class="form-control  <?php if (session('errors.question')) : ?>is-invalid<?php endif ?>" value="<?= !empty($faq) ? $faq->question : old('question') ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Urutan</label>
                            <input type="number" min="0" max="100" name="order" class="form-control  <?php if (session('errors.order')) : ?>is-invalid<?php endif ?>" value="<?= !empty($faq) ? $faq->order : old('order') ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Jawaban</label>
                    <textarea class="form-control  <?php if (session('errors.answer')) : ?>is-invalid<?php endif ?>" name="answer" id="answer" rows="10"><?= !empty($faq) ? $faq->answer : old('answer') ?></textarea>
                </div>
                <a class="btn btn-secondary" href="<?= base_url('resbim/faq'); ?>">Batal</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('js'); ?>
<script src="https://cdn.ckeditor.com/ckeditor5/28.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#answer'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<?= $this->endSection(); ?>