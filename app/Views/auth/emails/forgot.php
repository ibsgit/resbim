<table cellpadding=0 cellspacing=0 style="border-collapse:collapse;border-spacing:0;padding:0;margin:0;width:100%;height:100%;background-repeat:repeat;background-position:50% 0" width=100%>
    <tr style=border-collapse:collapse>
        <td style=padding:0;margin:0 valign=top>
            <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;width:100%;table-layout:fixed align=center class=mm-content>
                <tr style=border-collapse:collapse>
                    <td style=padding:0;margin:0;background-color:#fafafa align=center bgcolor=#fafafa>
                        <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;background-color:#fafafa width=600 align=center bgcolor=#fafafa>
                            <tr style=border-collapse:collapse>
                                <td style="margin:0;padding:20px 20px 5px;background-position:0 0" align=left>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=center width=560 valign=top>
                                                <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:0 0 5px;margin:0;line-height:14px;font-size:12px;color:#ccc" align=center class=mm-infoblock>
                                                            <p style="margin:0;font-size:12px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:14px;color:#ccc"><br>
                                                </table>
                                    </table>
                        </table>
            </table>
            <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;width:100%;table-layout:fixed align=center class=mm-content>
                <tr style=border-collapse:collapse>
                    <td style=padding:0;margin:0;background-color:#fafafa align=center bgcolor=#fafafa>
                        <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;background-color:#fff width=600 align=center bgcolor=#ffffff>
                            <tr style=border-collapse:collapse>
                                <td style="margin:0;padding:10px 20px 15px;border-radius:10px 10px 0 0;background-color:transparent;background-position:0 0" align=left bgcolor=transparent>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=center width=560 valign=top>
                                                <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:25px 0 20px;margin:0" align=center><img alt="" style=display:block;border:0;outline:0;text-decoration:none width=172>
                                                </table>
                                    </table>
                            <tr style=border-collapse:collapse>
                                <td style="padding:40px 20px 0;margin:0;background-color:transparent;background-position:0 0" align=left bgcolor=transparent>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=center width=560 valign=top>
                                                <table cellpadding=0 cellspacing=0 style="border-collapse:collapse;border-spacing:0;background-position:0 0" width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:15px 0;margin:0" align=center>
                                                            <h1 style="margin:0;line-height:24px;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:20px;font-style:normal;font-weight:400;color:#333"><strong style=background-color:transparent>ANDA LUPA PASSWORD?</strong></h1>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:0 40px;margin:0" align=center>
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">Selamat Datang,
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:0 35px 0 40px;margin:0" align=center>
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">Sepertinya Anda ingin melakukan reset password?
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:25px 35px 0;margin:0" align=center>
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">Jika Anda merasa tidak ingin melakukan reset <span class=il>abaikan</span> email ini. Untuk dapat reset password silahkan klik tombol di bawah ini:
                                                    <tr style=border-collapse:collapse>
                                                        <td style="margin:0;padding:40px 10px" align=center bgcolor=transparent><span class="mm-button-border mm-button-border-2" style=border-style:solid;border-color:#fff;background:#212c5f;border-width:2px;display:inline-block;border-radius:10px;width:auto><a class="mm-button mm-button-1" href="<?= site_url('reset-password') . '?token=' . $hash ?>" style="text-decoration:none;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;color:#fff;border-style:solid;border-color:#212c5f;border-width:20px 30px;display:inline-block;background:#212c5f;border-radius:10px;font-weight:700;font-style:normal;line-height:17px;width:auto;text-align:center" target=_blank>Reset Password</a></span>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:0 40px;margin:0" align=center>
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">Demi keamanan akun Anda,
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">segera lakukan reset passord.
                                                            <p style="margin:0;font-size:16px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:24px;color:#666">Tombol di atas hanya berlaku selama 1 x 24 jam.
                                                </table>
                                    </table>
                            <tr style=border-collapse:collapse>
                                <td style="padding:25px 0 0;margin:0;border-radius:0 0 10px 10px;background-color:#fff;background-position:0 0" align=left bgcolor=#ffffff>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;float:left align=left class=mm-left>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=left width=153>
                                                <table cellpadding=0 cellspacing=0 style="border-collapse:collapse;border-spacing:0;background-position:0 0" width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style=padding:0;margin:0 align=center><img alt="" style=display:block;border:0;outline:0;text-decoration:none width=133>
                                                    <tr style=border-collapse:collapse class=mm-mobile-hidden>
                                                </table>
                                            <td style=padding:0;margin:0 width=10 class=mm-hidden>
                                    </table>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;float:left align=left class=mm-left>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=left width=177>
                                                <table cellpadding=0 cellspacing=0 style="border-collapse:collapse;border-spacing:0;background-position:50% 50%" width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style=padding:0;margin:0 align=center class=mm-m-txt-c><img alt="" style=display:block;border:0;outline:0;text-decoration:none width=137>
                                                    <tr style=border-collapse:collapse>
                                                        <td style="padding:10px 0 5px;margin:0" align=left class=mm-m-txt-c>
                                                            <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 class="mm-social mm-table-not-adapt">
                                                                <tr style=border-collapse:collapse>
                                                            </table>
                                                </table>
                                    </table>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;float:right align=right class=mm-right>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=left width=250>
                                                <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style=padding:0;margin:0 align=right><img alt="" style=display:block;border:0;outline:0;text-decoration:none width=250>
                                                </table>
                                    </table>
                        </table>
            </table>
            <table cellpadding=0 cellspacing=0 style="border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;background-repeat:repeat;background-position:50% 0;table-layout:fixed" align=center class=mm-footer>
                <tr style=border-collapse:collapse>
                    <td style=padding:0;margin:0;background-color:#fafafa align=center bgcolor=#fafafa>
                        <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;background-color:transparent width=600 align=center bgcolor=transparent class=mm-footer-body>
                            <tr style=border-collapse:collapse>
                                <td style="margin:0;padding:15px 10px 5px;background-position:0 0;background-color:transparent" align=left bgcolor=transparent>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=center width=580 valign=top>
                                                <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style=padding:0;margin:0 align=center>
                                                            <p style="margin:0;font-size:12px;font-family:helvetica,'helvetica neue',arial,verdana,sans-serif;line-height:18px;color:#666">Email ini dikirim secara otomatis oleh sistem. Anda tidak perlu membalas atau mengirim email ke alamat ini.
                                                </table>
                                    </table>
                        </table>
            </table>
            <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;width:100%;table-layout:fixed align=center class=mm-content>
                <tr style=border-collapse:collapse>
                    <td style=padding:0;margin:0 align=center>
                        <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0;background-color:transparent width=600 align=center bgcolor=#ffffff>
                            <tr style=border-collapse:collapse>
                                <td style="margin:0;padding:15px 20px 25px;background-position:0 0;background-color:transparent" align=left bgcolor=transparent>
                                    <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                        <tr style=border-collapse:collapse>
                                            <td style=padding:0;margin:0 align=center width=560 valign=top>
                                                <table cellpadding=0 cellspacing=0 style=border-collapse:collapse;border-spacing:0 width=100%>
                                                    <tr style=border-collapse:collapse>
                                                        <td style=padding:0;margin:0;line-height:14px;font-size:12px;color:#ccc align=center class=mm-infoblock><img alt="" style=display:block;border:0;outline:0;text-decoration:none width=225>
                                                </table>
                                    </table>
                        </table>
            </table>
</table>
