<?php if (session()->has('message')) : ?>
	<div class="alert alert-success iMessage py-1">
		<?= session('message') ?>
	</div>
<?php endif ?>

<?php if (session()->has('error')) : ?>
	<div class="alert alert-danger iMessage py-1">
		<?= session('error') ?>
	</div>
<?php endif ?>

<?php if (session()->has('errors')) : ?>
	<div class="alert alert-danger iMessage py-1">
		<?php foreach (session('errors') as $error) : ?>
			<p class="mb-0"><?= $error ?></p>
		<?php endforeach ?>
	</div>
<?php endif ?>
<script>
	setTimeout(function() {
		$('.iMessage').slideUp("normal", function() {
			$(this).remove();
		});
	}, 4000);
</script>