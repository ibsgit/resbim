<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('dashboard'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fab fa-cloudflare"></i>
        </div>
        <div class="sidebar-brand-text mx-3">e-Resbim</div>
    </a>
    <hr class="sidebar-divider my-0">

    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('dashboard'); ?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <?php if (in_groups('admin')) : ?>
        <hr class="sidebar-divider my-0">
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('list'); ?>">
                <i class="fas fa-fw fa-users"></i>
                <span>List Pengguna</span>
            </a>
        </li>
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('resbim/qna'); ?>">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>Data Konsultasi</span>
            </a>
        </li>
    <?php endif; ?>

    <?php if (in_groups('user')) : ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('resbim/qna'); ?>">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>Data Konsultasi</span>
            </a>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" href="<?= base_url('front/faq'); ?>">
                <i class="fas fa-fw fa-quote-left"></i>
                <span>Data Permasalahan (web)</span>
            </a>
        </li> -->
    <?php endif; ?>

    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('resbim/faq'); ?>">
            <i class="fas fa-fw fa-quote-left"></i>
            <span>Data Permasalahan</span>
        </a>
    </li>
    <!-- <?php if (in_groups('admin')) : ?>
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('resbim/category'); ?>">
                <i class="fas fa-fw fa-bars"></i>
                <span>Kategori</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('resbim/categoryDetail'); ?>">
                <i class="fas fa-fw fa-stream"></i>
                <span>Kategori (Detail)</span>
            </a>
        </li>
    <?php endif; ?> -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('profile'); ?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Profil Saya</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>