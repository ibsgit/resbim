<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Resbim | <?= $title; ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?php base_url(); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php base_url(); ?>/css/sb-admin-2.min.css" rel="stylesheet">
    <?= $this->renderSection('css'); ?>
</head>

<body id="page-top">

    <style>
        td {
            padding-top: 1px !important;
            padding-bottom: 1px !important;
        }
    </style>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?= $this->include('templates/sidebar'); ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('templates/topbar'); ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <?php if ((empty(user()->whatsapp) || empty(user()->fullname)) &&  current_url(true) != base_url('profile')) : ?>
                    <div class="alert alert-danger mx-3">
                        Anda belum melengkapi informasi
                        <?= (empty(user()->whatsapp) && empty(user()->fullname)) ? '<b>Whatsapp dan Nama Lengkap</b>' : ''; ?>
                        <?= (empty(user()->fullname)) ? '<b>Nama Lengkap</b>' : ''; ?>
                        <?= (empty(user()->whatsapp)) ? '<b>Whatsapp</b>' : ''; ?>
                        silakan lengkapi melalui halaman <a href="<?= base_url('/profile'); ?>">profil</a>
                    </div>
                <?php endif; ?>
                <?= $this->renderSection('konten'); ?>

                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website <?= date('Y'); ?></span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="<?php base_url(); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?php base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php base_url(); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php base_url(); ?>/js/sb-admin-2.min.js"></script>

    <script src="<?php base_url(); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php base_url(); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <?= $this->renderSection('js'); ?>


</body>

</html>