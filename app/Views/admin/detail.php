<?= $this->extend('templates/index'); ?>
<?= $this->section('konten'); ?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-6">
            <h1 class="h4 mb-4 text-gray-800"><?= $title; ?></h1>
        </div>
        <div class="col-md-6">
            <?= view('\App\Views\templates\_message_block') ?>
        </div>
    </div>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="card mb-3 p-2" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= base_url('/img/' . $user->user_image); ?>" alt="<?= $user->fullname; ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <form class="user" method="post">
                                <?= csrf_field() ?>
                                <input type="hidden" name="id" value="<?= @$user->userid; ?>">
                                <input type="hidden" name="crypt" value="<?= @$user->crypt; ?>">

                                <div class="form-group pb-0">
                                    <input type="text" class="form-control" name="username" disabled value="<?= @$user->username; ?>" placeholder="Full Name">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="email" class="form-control" name="email" value="<?= $user->email; ?>" placeholder="Email">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="text" class="form-control" name="fullname" value="<?= @$user->fullname; ?>" placeholder="Full Name">
                                </div>
                                <div class="form-group pb-0">
                                    <input type="number" class="form-control <?php if (session('errors.whatsapp')) : ?>is-invalid<?php endif ?>" name="whatsapp" value="<?= $user->whatsapp; ?>" placeholder="No. Whatsapp">
                                </div>

                                <div class="form-group pb-0">
                                    <select class="form-control" name="group">
                                        <?php foreach ($groups as $group) : ?>
                                            <option value="<?= $group->id; ?>" <?php if ($user->groupid == $group->id) : ?>selected<?php endif ?>><?= $group->name . ' | ' . $group->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <a href="<?= base_url('list'); ?>" class="btn btn-secondary">&laquo; Kembali</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>