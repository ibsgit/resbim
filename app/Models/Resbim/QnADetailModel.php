<?php

namespace App\Models\Resbim;

use CodeIgniter\Model;

class QnADetailModel extends Model
{
    protected $table = 'resbim_qna_detail';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $allowedFields = [
        'userid', 'qnaid', 'content'
    ];

    protected $useTimestamps = true;

    protected $validationRules = [
        'content' => 'required',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    //--------------------------------------------------------------------
    // Category
    //--------------------------------------------------------------------
}
