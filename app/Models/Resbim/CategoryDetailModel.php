<?php

namespace App\Models\Resbim;

use CodeIgniter\Model;

class CategoryDetailModel extends Model
{
    protected $table = 'resbim_category_detail';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $allowedFields = [
        'name', 'order', 'categoryid'
    ];

    protected $useTimestamps = true;

    protected $validationRules = [
        'name' => 'required|max_length[255]|is_unique[resbim_category.name,name,{name}]',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    //--------------------------------------------------------------------
    // Category
    //--------------------------------------------------------------------
}
