<?php

namespace App\Models\Auth;

use Myth\Auth\Models\UserModel as MythUserModel;

class UserModel extends MythUserModel
{
    protected $allowedFields = [
        'email', 'username', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'permissions', 'deleted_at',
        'firstname', 'lastname', 'phone', 'fullname', 'user_image', 'whatsapp',
    ];
}
