<?php
function encrypt($string)
{

    $config         = new \Config\Encryption();
    $config->key    = 'aBigsecret_ofAtleast32Characters';
    $config->driver = 'OpenSSL';
    $encrypter = \Config\Services::encrypter($config);
    return strtr(base64_encode($encrypter->encrypt($string)), '+/=', '._-');
}
function decrypt($string)
{

    $config         = new \Config\Encryption();
    $config->key    = 'aBigsecret_ofAtleast32Characters';
    $config->driver = 'OpenSSL';
    $encrypter = \Config\Services::encrypter($config);
    return $encrypter->decrypt(base64_decode(strtr($string, '._-', '+/=')));
}
