<?php
// function sendWA($phone = '6285281407755', $message = 'test', $image = null)
// {
//     $curl = curl_init();

//     if (empty($image)) {
//         $dataSend = array('phone' => $phone, 'type' => 'text', 'text' => $message, 'delay' => '1', 'delay_req' => '1', 'schedule' => '0');
//     } else {
//         $dataSend = array('phone' => $phone, 'type' => 'image', 'caption' => $message, 'url' => $image,  'delay' => '1', 'delay_req' => '1', 'schedule' => '0');
//     }
//     curl_setopt_array($curl, array(
//         CURLOPT_URL => "https://fonnte.com/api/send_message.php",
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_ENCODING => "",
//         CURLOPT_MAXREDIRS => 10,
//         CURLOPT_TIMEOUT => 0,
//         CURLOPT_FOLLOWLOCATION => true,
//         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//         CURLOPT_CUSTOMREQUEST => "POST",
//         CURLOPT_POSTFIELDS => $dataSend,
//         CURLOPT_HTTPHEADER => array(
//             "Authorization: T724VKj2UGyBQ3N28MZ3"
//         ),
//     ));

//     $response = curl_exec($curl);

//     curl_close($curl);
//     return $response;
// }


// function whatspie($phone, $message, $image = null)
// {
//     echo $phone . ' ' . $message . ' ' . $image;
//     $device = '6285640334455';
//     $curl = curl_init();

//     if (empty($image)) {
//         $dataSend = array('device' => $device, 'receiver' => $phone, 'type' => 'chat', 'message' => $message,);
//     } else {
//         $dataSend = array('device' => $device, 'receiver' => $phone, 'type' => 'image', 'message' => $message, 'file_url' => $image,);
//     }
//     curl_setopt_array($curl, array(
//         CURLOPT_URL => "https://app.whatspie.com/api/messages",
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_ENCODING => "",
//         CURLOPT_MAXREDIRS => 10,
//         CURLOPT_TIMEOUT => 0,
//         CURLOPT_FOLLOWLOCATION => true,
//         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//         CURLOPT_CUSTOMREQUEST => "POST",
//         CURLOPT_POSTFIELDS => http_build_query($dataSend),
//         CURLOPT_HTTPHEADER => array(
//             "Accept: application/json",
//             "Content-Type: application/x-www-form-urlencoded",
//             "Authorization: Bearer x0hdJiu5dDT65LJzWBzsiJwpLZCZP3ynxTn74fSNHIg67QXTVO",
//         ),
//     ));

//     $response = curl_exec($curl);

//     curl_close($curl);
//     return $response;
// }

// This Script Recode By Mikel Yonathan : 6285157513313
// - ClassWhatsapp Wapisender.com
// - Pembuatan BOT ? PM 6285157513313 Pihak Ke 3 Wapisender.com

function sendWA($phone = '6285281407755', $message = 'test', $image = null)
{
    $curl = curl_init();

    if (empty($image)) {

        $dataSend = array(
            'key' => 'tDcG3VHP6qQgH2mw5fHvPw9InuTFesaw',
            'device' => '4bw0ds',
            'destination' => $phone,
            'message' => $message
        );
    } else {

        $dataSend = array(
            'key' => 'tDcG3VHP6qQgH2mw5fHvPw9InuTFesaw',
            'device' => '4bw0ds',
            'destination' => $phone,
            'message' => $message
        );
    }
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://wapisender.com/api/v1/send-message",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $dataSend,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $json = json_decode($response, true);
    return $json;
}

function CurlSend($action, $parameter)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://wapisender.com/api/v1/" . $action . "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
    $result = curl_exec($ch);
    $json = json_decode($result, true);
    return $json;
}

class WAPISENDER
{
    function __construct($data)
    {
        $this->key = $data['key'];
        $this->device = $data['device'];
    }

    public function SendMessage($pesan, $nomer)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'message' => $pesan
        );
        $post = CurlSend('send-message', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function sendMessageGroup($group_id, $text)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'group_id' => $group_id,
            'message' => $text
        );
        $post = CurlSend('send-message', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function SendImage($nomer, $source, $filename, $caption)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'image' => base64_encode(file_get_contents($source)),
            'filename' => $filename,
            'caption' => $caption
        );
        $post = CurlSend('send-image', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function SendVideo($nomer, $source, $filename, $caption)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'video' => base64_encode(file_get_contents($source)),
            'filename' => $filename,
            'caption' => $caption
        );
        $post = CurlSend('send-image', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }

    public function SendAudio($nomer, $audio, $filename)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'audio' => base64_encode(file_get_contents($audio)),
            'filename' => $filename
        );
        $post = CurlSend('send-audio', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function SendDoc($nomer, $doc, $filename, $caption)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'document' => base64_encode(file_get_contents($doc)),
            'filename' => $filename,
            'caption' => $caption
        );
        $post = CurlSend('send-document', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function SendLoc($nomer, $lat, $long, $address)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'destination' => $nomer,
            'lat' => $lat,
            'long' => $long,
            'address' => $address
        );
        $post = CurlSend('send-location', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function AddMember($group, $nomer)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'group_id' => $group,
            'contact_number' => $nomer,
            'action' => "add",
        );
        $post = CurlSend('group-member', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
    public function KickMember($group, $nomer)
    {
        $request = array(
            'key' => $this->key,
            'device' => $this->device,
            'group_id' => $group,
            'contact_number' => $nomer,
            'action' => "kick",
        );
        $post = CurlSend('group-member', $request);
        if ($post == true) {
            return $post;
        } else {
            return $post['message'];
        }
    }
}
